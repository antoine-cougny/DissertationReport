\newacronym{mtsp}{M-TSP}{Multi-Travelling Salesman Problem}
\newacronym{tsp}{TSP}{Travelling Salesman Problem}
\newacronym{ros}{ROS}{Robotic Operating System}
\newacronym{sab}{SAB}{System Architecture Blank}

\glsreset{ros}

\section{System Analysis}
\label{sec:overview}

% intro: Scenario / market model, robotic archi, blockchain archi
This section will present the system in its globality, by explaining first the
scenario which influenced the design of the system in
\cref{sec:designScenarioMarket}. Then the robotic architecture will be
presented to understand how robots operate in \cref{sec:roboticDesign} and the
{\DH}App design will be discussed in \cref{sec:dappDesign}. Finally, an
overview of the global system will be discussed in
\cref{sec:designOverviewSystem}.

%% -------------------------------------------------------------------------- %%
\subsection{Design of the Scenario and the Market Model}
\label{sec:designScenarioMarket}

\subsubsection{Scenario: Multi-Travelling Salesman Problem}
\paragraph{Presentation}

The scenario with which we test the system is inspired by the \gls{mtsp}.
This problem consists in minimizing the distance done by an agent whose main
goal is to visit a list of locations before going back to its initial position.
The main difference between the \gls{mtsp} and the \gls{tsp} is the number of
agents involved in the mission. In the case of the \gls{mtsp}, a generalization
of the \gls{tsp}, there are several agents with different starting points
\autocite{mTSP}, which have to visit a set of locations before going back to
their initial position. Solving this problem is done by taking the full team
into consideration.

In addition to this, we want the robots to perform measurements at the
different visited points, such as taking a picture, measuring the air
temperature or the brightness.

\paragraph{Simplified Implementation}

To make this problem interesting in the case of an open-market economy, the
agents do not know the full list of locations to visit before starting the
mission. Otherwise, they could just solve the problem using existing techniques
and then apply the plan for the whole team. Because the point of building a
market is to let the team members act in their self-best interest to increase
the global performance, having a dynamic announcement of locations to visit, 
emphasis market features such as response to unplanned events.

Performing a task has also been simplified to a simulated behaviour: robots
wait some time to the position they have been sent to. This information is
given by the operator at the moment and can be seen as an estimate of the
required cost to do measurements.

\paragraph{Reasons behind this Choice}

What makes this scenario interesting is how generic it is. More complex tasks
such as a security sweep
\autocite{HoplitesAMBAFrameworkForPlannedTightCoordinationMRS} or the box
pushing mission \autocite{AuctionMethodsForMultirobotCoordination} can be
decomposed as a set of goal positions and actions to perform at these
locations. This scenario also emphasizes robot mobility in a dynamic
environment. For instance, robots can interfere with each other even while
achieving a mission as basic as the one above. Lastly, this lets robots create
data which we want to be securely stored.  A possible implementation of this
secure storage is discussed in \cref{sec:ipfsApp}.

\subsubsection{Market Model: Single Round Auction}

\paragraph{Presentation}

For simplicity and speed purposes, a \textit{sealed first-price auction} has
been implemented. It consists of a single round auction in which all
participants submit their bid at the same time without knowing each other's bid
and the highest bidder wins the auction. This is the auction mechanism
implemented in MURDOCH by \textcite{AuctionMethodsForMultirobotCoordination}.

\paragraph{Auction Protocol}

Here is a summary of the implemented auction protocol.
More details will be given in \cref{sec:roboticArchitecture}.

\begin{itemize}
    \item Auction Opening: every trading agents listens to a common channel
        in which anybody can publish an \verb+announcement+ message to start
        an auction. This message contains information such as the task being
        auctioned.
    \item Bidding: when an agent receives an \verb+announcement+ message, it
        analyzes the task to estimate its cost. Before submitting a bid, the
        agent creates a private channel to receive the result of the auction.
    \item Auction Closure: the auctioneer analyzes the received bids and select
        the highest one. Then, it contacts every participant to tell whether 
        they won or not. The winner replies with its \gls{pk} in order to record
        the transaction on the blockchain.
    \item Logging: At the stage, auction ends. The last step
        consists in sending the transaction information to the blockchain so
        that a record is kept securely.
\end{itemize}


%% -------------------------------------------------------------------------- %%
\subsection{Design of the Robotic Architecture}
\label{sec:roboticDesign}

\subsubsection{Presentation of ROS}

One of the main aspects of the design of this project was to reuse as much as
possible existing tools to focus on the build of the open-market on top of
them. Because any of the frameworks presented in \cref{sec:mrsLR} could be used
for our own purposes, we had to build one. To do so, we used \gls{ros}.
\gls{ros} is an operating system for robots which provides features such as
hardware abstraction and low-level device control, message transmission between
processes and package management (\textcite{rosIntroWiki}).  \gls{ros} calls
processes \textit{nodes} which can be grouped into \textit{packages} in order
to be easily shared.

In the context of this project, \gls{ros} provides the structure to easily
deploy the developed solution on robots such as the Turtlebot
\autocite{turtlebotMainPage} (a low-cost robot aimed at multi-robot research).
Moreover, it also provides useful development and simulation tools, like
Gazebo, a 3D dynamic simulator similar to games engines
\autocite{gazeboMainPage}.

\subsubsection{Trading Robots Architecture}

\begin{figure}[!h]
    \centering
    \includegraphics[width=.8\textwidth]{Capella/LCBD_Logical_System_ROS.png}
    \caption{System Function Breakdown Diagram}
    \label{fig:simplifiedROS}
\end{figure}

\Cref{fig:simplifiedROS} shows a division into four nodes of the open
market system. The highest level is the \verb+blockchain handler+ node which
transfers auction results from the \verb+trader+ to the {\DH}App (see
\cref{sec:dappDesign}). One step lower is the \verb+trader+ node which open
auctions and participates in other's auction. It works alongside the
\verb+metrics+ node which estimates the cost of a mission.  Finally is the
\verb+executer+ which receives tasks, checks if it is still interesting by
asking for an updated cost\footnote{We could have accepted multiple missions,
    some of them could not be interesting enough to perform them anymore} and
performs them.

This three-level architecture is highly flexible as the communication between
the different nodes uses messages based on a generic "task" message. Moreover,
it lets each node have a specific function.

%% -------------------------------------------------------------------------- %%
\subsection{Design of the Blockchain Side of the Project}
\label{sec:dappDesign}

\subsubsection{Presentation of the Network}

We mention \textit{hyperledger\_fabric} in \cref{sec:hyperledger_fabricApp}
which lets users create and manage a permisionned blockchain as they can limit
who has access to their chains. In the context of the different clustered
architectures discussed in \cref{sec:clusteringNetwork}, this would enable the
use of different clusters of networks at different scales.

However, in order to simplify the implementation of this design due to time
constraints, implementing a "private Ethereum blockchain" has been preferred.
It works with the same rules as the main network, the main difference relying
on the \textit{genesis block}\footnote{the first block of a blockchain}, which
is the first block mined on the network.

The main computer / server will mine blocks on the blockchain, while robots
will only be clients as they do not have the processing power required to run a
full blockchain node. \Cref{fig:serverRobots} shows the robots as clients of a
single server architecture.

\begin{figure}[!h]
    \centering
    \includegraphics[width=\textwidth]{serverRobots.png}
    \caption{Robots are Client of a Single ROS Server}
    \label{fig:serverRobots}
\end{figure}

\subsubsection{Presentation of the {\DH}App}

A {\DH}App is a distributed application stored on a blockchain such as Ethereum
\autocite{StateOfDApp}. It is usually able to interact with a user through a
web interface.

In our case, the {\DH}App let the user insert a new task (or location to
visit). These tasks are recorded on the blockchain as \textit{tokens} (see
\cref{sec:ercTokens}). This {\DH}App is connected to the ROS network to be
able to trigger auctions in order to sell a task the operator just inserted in
the system. Moreover, it displays how many tasks and ether each robot owns, as
well as information on the different tasks such as position, reward and,
status.

To sum up, this part of the system is a bridge between the user, the
robotic system and a blockchain node. \Cref{fig:dappBlock} shows this
with a block for each major function. On the right is the user, on the left,
the robotic system and bellow is the blockchain.

\begin{figure}[!h]
    \centering
    \includegraphics[width=\textwidth]{Capella/LFBD_Root_Logical_Function_DApp.png}
    \caption{The {\DH}App is an interface between users, the blockchain and the \gls{mrs}}
    \label{fig:dappBlock}
\end{figure}

%% -------------------------------------------------------------------------- %%
\subsection{Design Overview of the System}
\label{sec:designOverviewSystem}

Having discussed the robotic architecture in \cref{sec:roboticDesign} and
of the {\DH}App in \cref{sec:dappDesign}, this section aims at describing the
whole system from the Arcadia Method point of view.

\Cref{fig:sab_deployChain} is the \gls{sab} Diagram. Capella describes
this diagram as being aimed at \textit{"Allocat[ing] System Functions to System and
    Actors"}. The additional information it provides consists in the link
between the different activities that were presented in \cref{fig:oab}. These
\textit{System Functions} are linked together by \textit{Functional Exchanges}
which indicate the nature of the link (ie. which information is
transmitted).

\begin{figure}[!h]
    \centering
    \vspace*{0.5cm}
    \advance\leftskip-2.8cm
    \includegraphics[width=20cm]{Capella/SAB_System_Analysis_Blank_ChainDeployment.png}
    \caption{SAB - System Analysis Blank Diagram with a Functional Chain
        highlighting the Deployment of a Task}
    \label{fig:sab_deployChain}
\end{figure}

The blue line on the figure is a Functional Chain, which is a sequence of linked
activities. This one corresponds to a scenario in which, the operator inputs a
new task to be done and unfolds as:
\begin{itemize}[noitemsep]
    \item The task is registered on the blockchain.
    \item An auction is opened and the task is sent for a cost analysis by the robots.
    \item Robots bid on the task.
    \item The auctioneer selects the best bid and contacts the winner.
    \item The winner stores the task in memory. The auction is done.
    \item When the task is selected to be performed, its cost is checked to 
        make sure it is still interesting.
    \item After the task is done, a signal is sent to record its achievement.
\end{itemize}

\Cref{fig:es_deployChain} is the sequence diagram related to this scenario.
\Cref{fig:sab_reauctionChain} shows the same \gls{sab} diagram but with the
Functional Chain corresponding to a task re-auctioned because its cost became
too expensive.
